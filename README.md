# Archived tables Backup

This ansible script backs up archived mysql tables (prefixed with archived_) in AWS RDS and copies it to S3. It then trigers a notification to an SNS topic which in turn sends an email.

To run the script:
```
ansible-playbook -i hosts main.yml
```

The script is meant to run on a remote ec2 instance (Amazon Linux AMI) or CentOS 7. Please make sure that you can successfully login to the instance with the private ssh key. I have aliased the login instance as `aws`

Please ensure you meet the following conditions:

- Create user in `Identity and Access Management` (AWS) of `Programmatic access type` so it Enables an access key ID and secret access key with `AmazonS3FullAccess` and `AmazonSNSFullAccess` permissions. The access key and secret key are used to gain access to aws. Update the respective ansible variables in the vars directive.
- Update the vars file with the respective credentials. Make sure the mysql RDS instance is accessible from the EC2 instance used by ansible. Update the mysql_* variables with the RDS instance details
- Make sure that once the ansible script creates a topic and adds the emails to the subscription, verify the emails using the link sent to them.

Once the script logs in, it will install required dependencies and proceed to back up the archived tables, delete them, upload the backup file to s3 and triggers a notification to the topic.
